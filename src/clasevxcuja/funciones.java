/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasevxcuja;

import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.WritableRaster;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import static org.opencv.core.Core.bitwise_not;
import static org.opencv.core.Core.bitwise_or;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author oscar
 */
public class funciones {
    public static double bwarea(Mat im) { //Esta no va bien, borrar. DEPRECATED

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(im, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        
        double area = 0;
        for (int i = 0; i < contours.size(); i++) {
            area = area + Imgproc.contourArea(contours.get(i));
        }
        return (area);
    }
    public static Mat bwareaopen(Mat im, double size) { //Esta no va bien, borrar. DEPRECATED
        Mat salida = new Mat().zeros(new Size(im.width(), im.height()), CvType.CV_8U);

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(im, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        for (int i = 0; i < contours.size(); i++) {
            double area = Imgproc.contourArea(contours.get(i));

            if (area > size) {
                Scalar color = new Scalar(255);
                Imgproc.drawContours(salida, contours, i, color, -1);
            }
        }
        Imgproc.threshold(salida, salida, 1, 255, Imgproc.THRESH_BINARY);
        return (salida);
    }
    
    public static Mat imfill(Mat im, String modo) {
        Mat imSalida = new Mat().zeros(im.height(), im.width(), CvType.CV_8U);
        switch (modo) {
            case "holes":
                Mat mask = Mat.zeros(im.height() + 2, im.width() + 2, CvType.CV_8U);
                Mat im_temp = im.clone();

                Point punto = new Point(0, 0);
                Scalar valor = new Scalar(255);

                Imgproc.floodFill(im_temp, mask, punto, valor);

                bitwise_not(im_temp, im_temp);
                bitwise_or(im_temp, im, imSalida);

                return imSalida;
            case "holes2":
                List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
                Imgproc.findContours(im, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

                for (int i = 0; i < contours.size(); i++) {
                    Scalar color = new Scalar(255);
                    Imgproc.drawContours(imSalida, contours, i, color, -1);
                }
                return (imSalida);
        }
        return (imSalida);
    }
    public static Mat img2Mat(BufferedImage image) {
        byte[] data;
        try {
            data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();

        } catch (Exception e) {
            int[] ints = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

            BufferedImage image2 = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
            WritableRaster raster = (WritableRaster) image2.getData();

            raster.setPixels(0, 0, image.getWidth(), image.getHeight(), ints);

            data = ((DataBufferByte) image2.getRaster().getDataBuffer()).getData();
//            data = new byte[ints.length];
//            for (int i=0; i<ints.length; i++){
//                data[i] = (byte)ints[i];
//            }
        }

        if (image.getHeight() * image.getWidth() == data.length) {
            Mat mat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC1);
            mat.put(0, 0, data);
            return mat;
        } else {
            Mat mat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
            mat.put(0, 0, data);
            return mat;
        }

    }
    public static void imshow(Mat im) {
               
            Mat im2 = new Mat();
            Size size = new Size(Math.round(im.width() / 2), Math.round(im.height() / 2));
            Imgproc.resize(im, im2, size);
            BufferedImage imResultadosRes = Mat2BufferedImage(im2);
            displayImage("", imResultadosRes);
  
    }
    
    public static void displayImage(String text, Image img2) {        
        ImageIcon icon = new ImageIcon(img2);
        JFrame frame = new JFrame();
        frame.setTitle(text);
        frame.setLayout(new FlowLayout());
        frame.setSize(img2.getWidth(null) + 50, img2.getHeight(null) + 50);
        JLabel lbl = new JLabel();
        lbl.setIcon(icon);
        frame.add(lbl);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
    }
    
    public static BufferedImage Mat2BufferedImage(Mat m) {

        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (m.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels() * m.cols() * m.rows();
        byte[] b = new byte[bufferSize];
        m.get(0, 0, b); // get all the pixels
        BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;

    }
    
}
